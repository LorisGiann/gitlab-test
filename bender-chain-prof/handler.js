'use strict'
const axios = require('axios');

module.exports = async (event, context) => {
  const result = {
    'body': JSON.stringify(event.body),
    'content-type': event.headers["content-type"]
  }

  var resfn1 = axios.post('http://192.168.56.102:8080/function/bender-fn', "ciao");
  var resfn2 = axios.post('http://192.168.56.102:8080/function/bender2-fn', "mondo");


  var x = await Promise.all([resfn1, resfn2]).then(([a, b]) => "result" + JSON.stringify(a.data) + JSON.stringify(b.data)).catch(console.log);

  return context
    .status(200)
    .succeed(x)
}

